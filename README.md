# OpenAgenda

[![pipeline status](https://gitlab.com/opencontent/openagenda/badges/master/pipeline.svg)](https://gitlab.com/opencontent/openagenda/commits/master)
[![license](https://img.shields.io/badge/license-GPL-blue.svg)](https://gitlab.com/opencontent/openagenda/blob/master/LICENSE)

Calendario degli eventi partecipato dai cittadini

## Descrizione

![Homepage di OpenAgenda](https://www.opencontent.it/var/opencontent/storage/images/media/images/come-si-presenta-il-calendario-al-cittadino/2088-1-ita-IT/Come-si-presenta-il-calendario-al-cittadino_imagefullwide.png)
E’ un calendario eventi partecipato dai soggetti che animano la vita culturale 
e sociale di una comunità locale; associazioni, biblioteche e musei pubblicano 
i propri eventi in un unico calendario che ne facilita la diffusione multicanale, 
in coordinamento con l'ente locale; un modo per rafforzare le relazioni tra
amministrazione e territorio, secondo il paradigma dell'*OpenGovernment*.

Consente di prevenire le sovrapposizioni di date e di raggruppare le iniziative 
per tematiche in base agli interessi di cittadini e turisti, che vi accedono 
tramite pc, tablet, smartphone, o scaricando una locandina sintetica corredata 
di un QRcode per evento, generata automaticamente. 
I profili delle associazioni e gli stessi eventi vengono rilasciati come *Open Data*
e resi disponibili via API.

## Altri riferimenti

Per maggiori informazioni è possibile consultare: 

 * [Demo](https://openagenda.openpa.opencontent.io)
 * [Pagina ufficiale del prodotto](https://www.opencontent.it/Per-la-PA/OpenAgenda)
 * [Manuale utente](https://manuale-openagenda.readthedocs.io)

## API 

 * [API della Demo](https://openagenda.openpa.opencontent.io/api/opendata/)
 * [Documentazione API](https://documenter.getpostman.com/view/7046499/S17tPncK?version=latest)


## Setup

### Build

    docker build -t openagenda .

### Local docker environment

Fix permissions in directories `var` and `dfs` 

    sudo chown 33 var dfs

Add the following line to your `hosts` file:

    docker-compose up -d

To tail the logs of all containers

    docker-compose logs -f --tail 20

or just one of them

    docker-compose logs -f --tail 20 php

To launch a bash in the same container of a service (eg: php)

    docker-compose exec php bash

To pincionate in postgres cli:

    docker-compose exec postgres bash
    bash-4.4# psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB"

To pincionate in postgres from web open 
   
    * [ez20150103_openpa_prototipo](http://localtest.me:8081)  
    * [ez20150103_openpa_prototipo_dfs](http://localtest.me:8082)

### Rebuild database from scratch

To rebuild the database from scratch you need to stop the db container, remove it, remove the
volume `pgdata` and up the db container

    docker-compose stop postgres; \
    docker-compose rm postgres; \
    docker volume rm openagenda_pgdata; \
    docker-compose up postgres

Pay attention the volume name depends from the name of the directory containing the project, so if you customized it during the repository clone, change the volume accordingly

## Altri screenshot

![Gli eventi possono essere filtrati in base all'interesse del singolo cittadino](https://www.opencontent.it/var/opencontent/storage/images/media/images/solo-gli-eventi-di-interesse-per-il-cittadino-i-filtri/2091-1-ita-IT/Solo-gli-eventi-di-interesse-per-il-cittadino-i-filtri_imagefullwide.png)
![Esempio di pagina con mappa degli eventi](https://www.opencontent.it/var/opencontent/storage/images/media/images/navigazione-sulla-mappa-su-cui-applicare-i-filtri/2085-1-ita-IT/Navigazione-sulla-mappa-su-cui-applicare-i-filtri_imagefullwide.png)
![Interfaccia per la gestione degli eventi, vista da un operatore comunale](https://www.opencontent.it/var/opencontent/storage/images/media/images/un-cruscotto-per-moderare-e-monitorare-gli-eventi2/2070-1-ita-IT/Un-cruscotto-per-moderare-e-monitorare-gli-eventi_imagefullwide.png)
![Moderazione del singolo evento](https://www.opencontent.it/var/opencontent/storage/images/media/images/moderazione-dei-commenti-dei-partecipanti/2037-1-ita-IT/Moderazione-dei-commenti-dei-partecipanti_imagefullwide.png)
![L'indicazione del luogo di svolgimento dell'evento è facilitata dalla geolocalizzazione](https://www.opencontent.it/var/opencontent/storage/images/media/images/compilazione-guidata-dell-evento-anche-da-mobile/2049-1-ita-IT/Compilazione-guidata-dell-evento-anche-da-mobile_imagefullwide.png)
![Esempio di calendario stampabile creato automaticamente dal sito web](https://www.opencontent.it/var/opencontent/storage/images/media/images/locandina-automatica-con-eventi-dotati-di-qrcode2/2076-1-ita-IT/Locandina-automatica-con-eventi-dotati-di-QRcode_imagefullwide.png)
![Il calendario stampabile può essere personalizzato nei contenuti e nel formato](https://www.opencontent.it/var/opencontent/storage/images/media/images/impaginare-la-bruchure-via-web/2043-1-ita-IT/Impaginare-la-bruchure-via-web_imagefullwide.png)


![Elenco delle Associazioni registrate, visto da un operatore comunale](https://www.opencontent.it/var/opencontent/storage/images/media/images/il-registro-delle-associazioni-finalmente-in-ordine2/2073-1-ita-IT/Il-registro-delle-associazioni-finalmente-in-ordine_imagefullwide.png)


