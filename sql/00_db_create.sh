#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
CREATE DATABASE openagenda_prototipo ENCODING='utf8'; 
GRANT ALL PRIVILEGES ON DATABASE openagenda_prototipo TO openpa;
CREATE DATABASE openagenda_prototipo_dfs ENCODING='utf8';
GRANT ALL PRIVILEGES ON DATABASE openagenda_prototipo_dfs TO openpa;
EOSQL
