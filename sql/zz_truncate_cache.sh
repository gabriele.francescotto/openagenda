#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "ez20150103_openpa_prototipo_dfs" <<-EOSQL
TRUNCATE ezdfsfile_cache;
EOSQL
