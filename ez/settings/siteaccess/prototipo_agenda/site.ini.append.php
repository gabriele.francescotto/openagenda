<?php /* #?ini charset="utf-8"?

[HTTPHeaderSettings]
CustomHeader=enabled
OnlyForAnonymous=disabled
OnlyForContent=enabled
Cache-Control[]
Cache-Control[/]=public, must-revalidate, max-age=259200, s-maxage=259200
HeaderList[]=Vary
Vary[/]=X-User-Context-Hash
HeaderList[]=X-Instance
X-Instance[/]=prototipo

[ExtensionSettings]
ActiveAccessExtensions[]=ezflow
ActiveAccessExtensions[]=ezgmaplocation
ActiveAccessExtensions[]=ezjscore
ActiveAccessExtensions[]=ezmultiupload
ActiveAccessExtensions[]=ezodf
ActiveAccessExtensions[]=ezoe
ActiveAccessExtensions[]=ezwt
ActiveAccessExtensions[]=ocmaintenance
ActiveAccessExtensions[]=occsvimport
ActiveAccessExtensions[]=openpa_importers
ActiveAccessExtensions[]=sqliimport
ActiveAccessExtensions[]=ocinigui
ActiveAccessExtensions[]=openpa
ActiveAccessExtensions[]=ezflowplayer
ActiveAccessExtensions[]=ezfind
ActiveAccessExtensions[]=ocsearchtools
ActiveAccessExtensions[]=ezflip
ActiveAccessExtensions[]=occhangeobjectdate
ActiveAccessExtensions[]=ocmediaplayer
ActiveAccessExtensions[]=jcremoteid
ActiveAccessExtensions[]=ggwebservices
ActiveAccessExtensions[]=batchtool
ActiveAccessExtensions[]=ocmap
ActiveAccessExtensions[]=ocmaps
ActiveAccessExtensions[]=ezprestapiprovider
ActiveAccessExtensions[]=ocopendata
ActiveAccessExtensions[]=ocexportas
ActiveAccessExtensions[]=ezchangeclass
ActiveAccessExtensions[]=ezclasslists
ActiveAccessExtensions[]=collectexport
ActiveAccessExtensions[]=eztags
ActiveAccessExtensions[]=ocextensionsorder
ActiveAccessExtensions[]=bcgooglesitemaps
ActiveAccessExtensions[]=ocembed
ActiveAccessExtensions[]=bfsurveyfile
ActiveAccessExtensions[]=mugosurvey_addons
ActiveAccessExtensions[]=ezsurvey
ActiveAccessExtensions[]=ezstarrating
ActiveAccessExtensions[]=enhancedezbinaryfile
ActiveAccessExtensions[]=ocrss
ActiveAccessExtensions[]=ocrecaptcha
ActiveAccessExtensions[]=occhangeloginname
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=openpa_theme_2014
ActiveAccessExtensions[]=ocbootstrap
ActiveAccessExtensions[]=ocsocialuser
ActiveAccessExtensions[]=ocsocialdesign
ActiveAccessExtensions[]=openpa_agenda
ActiveAccessExtensions[]=oceditorialstuff
ActiveAccessExtensions[]=ngopengraph
ActiveAccessExtensions[]=ngpush
ActiveAccessExtensions[]=ocbinarynullparser
ActiveAccessExtensions[]=ocmultibinary
ActiveAccessExtensions[]=mugoobjectrelations
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=ocfoshttpcache
ActiveAccessExtensions[]=easyvocs_connector
ActiveAccessExtensions[]=ocsupport
ActiveAccessExtensions[]=ezuserformtoken
ActiveAccessExtensions[]=ocgdprtools
ActiveAccessExtensions[]=ezmbpaex

[DatabaseSettings]
DatabaseImplementation=ezpostgresql
Server=postgres
Port=
User=openpa
Password=openp4ssword
Database=openagenda_prototipo
Charset=utf-8
Socket=disabled
SQLOutput=disabled

[Session]
SessionNamePerSiteAccess=disabled

[SiteSettings]
SiteName=Comune di OpenContent
SiteURL=openagenda.openpa.opencontent.io
LoginPage=embedded
IndexPage=agenda/home
DefaultPage=agenda/home

[SiteAccessSettings]
RequireUserLogin=false
ShowHiddenNodes=false
RelatedSiteAccessList[]
RelatedSiteAccessList[]=prototipo_frontend
RelatedSiteAccessList[]=prototipo_debug
RelatedSiteAccessList[]=prototipo_backend
RelatedSiteAccessList[]=prototipo_agenda
RelatedSiteAccessList[]=prototipo_booking
RelatedSiteAccessList[]=prototipo_sensor
RelatedSiteAccessList[]=prototipo_dimmi
RelatedSiteAccessList[]=prototipo_trasparenza

[DesignSettings]
SiteDesign=designitalia
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=agenda
AdditionalSiteDesignList[]=social
AdditionalSiteDesignList[]=ocbootstrap
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=ita-IT
ContentObjectLocale=ita-IT
ShowUntranslatedObjects=disabled
SiteLanguageList[]
SiteLanguageList[]=ita-IT
TextTranslation=enabled
TranslationSA[ita]=Ita

[FileSettings]
VarDir=var/prototipo

[MailSettings]
AdminEmail=webmaster@opencontent.it
EmailSender=

[InformationCollectionSettings]
EmailReceiver=

[UserSettings]
RegistrationEmail=

[ContentSettings]
TranslationList=

[SiteAccessRules]
Rules[]
Rules[]=access;enable
Rules[]=moduleall
Rules[]=access;disable
Rules[]=module;ezinfo/about
Rules[]=module;setup/extensions
Rules[]=module;content/tipafriend
Rules[]=module;settings/edit

*/ ?>
