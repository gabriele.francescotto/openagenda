<?php /* #?ini charset="utf-8"?

[HTTPHeaderSettings]
CustomHeader=enabled
OnlyForAnonymous=disabled
OnlyForContent=enabled
Cache-Control[]
Cache-Control[/]=public, must-revalidate, max-age=259200, s-maxage=259200
HeaderList[]=Vary
Vary[/]=X-User-Context-Hash
HeaderList[]=X-Instance
X-Instance[/]=prototipo

[DatabaseSettings]
DatabaseImplementation=ezpostgresql
Server=postgres
Port=
User=openpa
Password=openp4ssword
Database=openagenda_prototipo
Charset=utf-8
Socket=disabled
SQLOutput=disabled

[SiteSettings]
SiteName=Comune di OpenContent
SiteURL=saasopenpa.opencontent.it/backend
DefaultPage=content/dashboard
LoginPage=custom

[SiteAccessSettings]
RequireUserLogin=true
ShowHiddenNodes=true
RelatedSiteAccessList[]
RelatedSiteAccessList[]=prototipo_frontend
RelatedSiteAccessList[]=prototipo_debug
RelatedSiteAccessList[]=prototipo_backend
RelatedSiteAccessList[]=prototipo_agenda
RelatedSiteAccessList[]=prototipo_booking
RelatedSiteAccessList[]=prototipo_sensor
RelatedSiteAccessList[]=prototipo_dimmi
RelatedSiteAccessList[]=prototipo_trasparenza

[DesignSettings]
SiteDesign=backend
AdditionalSiteDesignList[]
AdditionalSiteDesignList[]=admin2
AdditionalSiteDesignList[]=admin
AdditionalSiteDesignList[]=ezflow
AdditionalSiteDesignList[]=standard

[RegionalSettings]
Locale=ita-IT
ContentObjectLocale=ita-IT
ShowUntranslatedObjects=enabled
SiteLanguageList[]=ita-IT
TextTranslation=enabled

[FileSettings]
VarDir=var/prototipo

[ContentSettings]
CachedViewPreferences[full]=admin_navigation_content=1;admin_children_viewmode=list;admin_list_limit=1
TranslationList=

[MailSettings]
AdminEmail=webmaster@opencontent.it
EmailSender=

[UserSettings]
RegistrationEmail=

[InformationCollectionSettings]
EmailReceiver=

[ExtensionSettings]

ActiveAccessExtensions[]=ezflow
ActiveAccessExtensions[]=ezgmaplocation
ActiveAccessExtensions[]=ezjscore
ActiveAccessExtensions[]=ezmultiupload
ActiveAccessExtensions[]=ezodf
ActiveAccessExtensions[]=ezoe
ActiveAccessExtensions[]=ezwt
ActiveAccessExtensions[]=ocmaintenance
ActiveAccessExtensions[]=occsvimport
ActiveAccessExtensions[]=openpa_importers
ActiveAccessExtensions[]=sqliimport
ActiveAccessExtensions[]=ocinigui
ActiveAccessExtensions[]=openpa
ActiveAccessExtensions[]=ezflowplayer
ActiveAccessExtensions[]=ezfind
ActiveAccessExtensions[]=ocsearchtools
ActiveAccessExtensions[]=ezflip
ActiveAccessExtensions[]=occhangeobjectdate
ActiveAccessExtensions[]=ocmediaplayer
ActiveAccessExtensions[]=jcremoteid
ActiveAccessExtensions[]=ggwebservices
ActiveAccessExtensions[]=batchtool
ActiveAccessExtensions[]=ocmap
ActiveAccessExtensions[]=ocmaps
ActiveAccessExtensions[]=ezprestapiprovider
ActiveAccessExtensions[]=ocopendata
ActiveAccessExtensions[]=ocexportas
ActiveAccessExtensions[]=ezchangeclass
ActiveAccessExtensions[]=ezclasslists
ActiveAccessExtensions[]=collectexport
ActiveAccessExtensions[]=eztags
ActiveAccessExtensions[]=ocextensionsorder
ActiveAccessExtensions[]=bcgooglesitemaps
ActiveAccessExtensions[]=ocembed
ActiveAccessExtensions[]=bfsurveyfile
ActiveAccessExtensions[]=mugosurvey_addons
ActiveAccessExtensions[]=ezsurvey
ActiveAccessExtensions[]=ezstarrating
ActiveAccessExtensions[]=enhancedezbinaryfile
ActiveAccessExtensions[]=ocrss
ActiveAccessExtensions[]=ocwhatsapp
ActiveAccessExtensions[]=ocrecaptcha
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=ocsocialuser
ActiveAccessExtensions[]=ocsocialdesign
ActiveAccessExtensions[]=ocsensor
ActiveAccessExtensions[]=openpa_sensor
ActiveAccessExtensions[]=openpa_dimmi
ActiveAccessExtensions[]=openpa_agenda
ActiveAccessExtensions[]=openpa_booking
ActiveAccessExtensions[]=oceditorialstuff
ActiveAccessExtensions[]=cjw_newsletter
ActiveAccessExtensions[]=openpa_newsletter
ActiveAccessExtensions[]=ocbinarynullparser
ActiveAccessExtensions[]=ocmultibinary
ActiveAccessExtensions[]=mugoobjectrelations
ActiveAccessExtensions[]=ocoperatorscollection
ActiveAccessExtensions[]=ocfoshttpcache
ActiveAccessExtensions[]=easyvocs_connector
ActiveAccessExtensions[]=ocsupport
ActiveAccessExtensions[]=ezuserformtoken
ActiveAccessExtensions[]=ocgdprtools
ActiveAccessExtensions[]=ezmbpaex
ActiveAccessExtensions[]=ocalboonline

[SiteAccessRules]
Rules[]
Rules[]=access;enable
Rules[]=moduleall
Rules[]=access;disable
Rules[]=module;ezinfo/about
Rules[]=module;setup/extensions
Rules[]=module;content/tipafriend
Rules[]=module;settings/edit
Rules[]=module;user/register
*/ ?>