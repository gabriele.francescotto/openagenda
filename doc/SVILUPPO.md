## Sviluppo in ambiente Mac OS X con MAMP

 * Creare cartella `/mnt/efs/cluster-openpa/`

 * Clonare e installare il repository del codice
```
git clone https://git-codecommit.eu-west-1.amazonaws.com/v1/repos/saasopenpa-distribution
composer install --ignore-platform-reqs --prefer-source
```

 * Clonare e installare il repository di solr
 ```
git clone https://git-codecommit.eu-west-1.amazonaws.com/v1/repos/solr-saasopenpa
cd solr-saasopenpa/java
sudo sh solr.sh
```

 * Impostare i domini astratti in /etc/hosts
```
127.0.0.1 db.saasopenpa-astratto java.saasopenpa-astratto varnish.saasopenpa-astratto
```

 * Installare e lanciare varnish 4
```
brew install automake
brew install libtool
brew install pcre
brew install wget
brew install docutils
cd $HOME
wget http://varnish-cache.org/_downloads/varnish-4.1.10.tgz
tar xvzf varnish-4.1.10.tar.gz
cd varnish-4.1.10
./configure --enable-debugging-symbols --enable-developer-warnings --enable-dependency-tracking
make
make install

sudo /usr/local/sbin/varnishd -a 127.0.0.1:80 -T 127.0.0.1:6082 -f default.vcl -s file,/tmp,500M

# per killarlo
# sudo pkill varnishd
```


 * Recuperare i dump di un istanza dalla produzione
```
# accedere alla macchina cron di produzione
ssh <ip-ec2-cron>

pg_dump --host=db.saasopenpa-astratto --port=5432 --username=openpa --password --dbname=ez20150103_openpa_<NOME_ISTANZA> > ez20150103_openpa_<NOME_ISTANZA>.sql;
pg_dump --host=db.saasopenpa-astratto --port=5432 --username=openpa --password --dbname=ez20150103_openpa_<NOME_ISTANZA>_dfs > ez20150103_openpa_<NOME_ISTANZA>_dfs.sql
exit

# copia in locale
scp centos@<ip-ec2-cron>:/home/centos/ez20150103_openpa_<NOME_ISTANZA>* .
```

 * Installare i dump in locale
```
sudo mv ez20150103_openpa_<NOME_ISTANZA>* /Library/PostgreSQL/9.1
sudo chown postgres:staff /Library/PostgreSQL/9.1/ez20150103_openpa_<NOME_ISTANZA>*

sudo su
su - postgres
psql
CREATE DATABASE ez20150103_openpa_<NOME_ISTANZA> ENCODING='utf8'; GRANT ALL PRIVILEGES ON DATABASE ez20150103_openpa_<NOME_ISTANZA> TO openpa;
CREATE DATABASE ez20150103_openpa_<NOME_ISTANZA>_dfs ENCODING='utf8'; GRANT ALL PRIVILEGES ON DATABASE ez20150103_openpa_<NOME_ISTANZA>_dfs TO openpa;
# uscire con ^AD

/Library/PostgreSQL/9.1/bin/psql -d ez20150103_openpa_<NOME_ISTANZA> -f ez20150103_openpa_<NOME_ISTANZA>.sql 2> errors.log
/Library/PostgreSQL/9.1/bin/psql -d ez20150103_openpa_<NOME_ISTANZA>_dfs -f ez20150103_openpa_<NOME_ISTANZA>_dfs.sql 2> errors.log
```

 * Se serve recuperare solo le immagini dalla produzione
```
php download_images.php -s<NOME_ISTANZA>_backend --url=<URL_ISTANZA>
```

 * Reindicizzare
```
php bin/php/updatesearchindex.php -s<NOME_ISTANZA>_backend
```

 * Impostare MAMP in ascolto sulla porta 8080 e creare l'host ```<URL_ISTANZA>```
 * Aggiungere in Apache Additional parameters for <VirtualHost> directive:
```
SetEnv ISTANZA <NOME_ISTANZA>
<IfModule mod_rewrite.c>
    RewriteEngine On

    RewriteRule  ^/var/([^/]+/)?storage/images-versioned/.*  /index_cluster.php [L]
    RewriteRule  ^/var/([^/]+/)?storage/images/.*  /index_cluster.php [L]
    RewriteRule   ^/var/([^/]+/)?cache/public/(stylesheets|javascript)  /index_cluster.php  [L]

    # REST API
    RewriteRule ^/api/ /index_rest.php [L]

    RewriteRule ^/([^/]+/)?content/treemenu.* /index_treemenu.php [L]
    RewriteRule ^/var/([^/]+/)?storage/images(-versioned)?/.* - [L]
    RewriteRule ^/var/([^/]+/)?cache/(texttoimage|public)/.* - [L]
    RewriteRule ^/design/[^/]+/(stylesheets|images|javascript|fonts)/.* - [L]
    RewriteRule ^/share/icons/.* - [L]
    RewriteRule ^/extension/[^/]+/design/[^/]+/(stylesheets|flash|images|lib|fonts|javascript|javascripts?)/.* - [L]
    RewriteRule ^/packages/styles/.+/(stylesheets|images|javascript)/[^/]+/.* - [L]
    RewriteRule ^/packages/styles/.+/thumbnail/.* - [L]
    RewriteRule ^/var/storage/packages/.* - [L]

    #  Makes it possible to placed your favicon at the root of your
    #  eZ Publish instance. It will then be served directly.
    RewriteRule ^/favicon\.ico - [L]
    #  Uncomment the line below if you want you favicon be served from the standard design.
    #  You can customize the path to favicon.ico by replacing design/standard/images/favicon.ico
    #  by the adequate path.
    #RewriteRule ^/favicon\.ico /design/standard/images/favicon.ico [L]
    RewriteRule ^/design/standard/images/favicon\.ico - [L]

    # Give direct access to robots.txt for use by crawlers (Google, Bing, Spammers..)
    RewriteRule ^/robots\.txt - [L]

    # Platform for Privacy Preferences Project ( P3P ) related files for Internet Explorer
    # More info here : http://en.wikipedia.org/wiki/P3p
    RewriteRule ^/w3c/p3p\.xml - [L]

    # Uncomment the following lines when using popup style debug.
    # RewriteRule ^/var/cache/debug\.html.* - [L]
    # RewriteRule ^/var/[^/]+/cache/debug\.html.* - [L]

    RewriteRule .* /index.php
</IfModule>
```

 * Navigare in ```<URL_ISTANZA>``` (in locale)

#### File utili all'installazione

default.vcl
```
vcl 4.0;

acl purge {
        "localhost";
}

backend default {
    .host = "127.0.0.1";
    .port = "8080";
}

sub vcl_recv {

    if( req.method == "PURGE" ) {
        # Limit access for security reasons
        if( !client.ip ~ purge ) {
            return( synth( 405, "Not allowed." ) );
        }

        if( req.http.X-Ban-Condition ) {
            ban( req.http.X-Ban-Condition );
            return( synth( 200, "Purged: " + req.http.X-Ban-Condition ) );
        }

        return( synth( 500, "Missing X-Ban-Condition header." ) );
    }

    if ( (req.http.host ~ "^(?i)sito.comune.trento.it") && req.http.X-Forwarded-Proto !~ "(?i)https") {
        set req.http.x-Redir-Url = "https://sito.comune.trento.it" + req.url;
        return(synth(850, ""));
    }

    // Don't cache private sites
    if (req.http.host ~ ".*\.intra.comune.trento.it") {
        return( pass );
    }

    // Don't cache requests other than GET and HEAD.
    if (req.method != "GET" && req.method != "HEAD") {
        return (pass);
    }

    // Normalize the Accept-Encoding headers
    if (req.http.Accept-Encoding) {
        if (req.http.Accept-Encoding ~ "gzip") {
            set req.http.Accept-Encoding = "gzip";
        } elsif (req.http.Accept-Encoding ~ "deflate") {
            set req.http.Accept-Encoding = "deflate";
        } else {
            unset req.http.Accept-Encoding;
        }
    }

    // Don't cache Authenticate & Authorization
    // You may remove this when using REST API with basic auth.
    if (req.http.Authenticate || req.http.Authorization) {
        //if (client.ip ~ debuggers) {
            set req.http.X-Debug = "Not Cached according to configuration (Authorization)";
        //}
        return (hash);
    }

    // Don't cache authenticated sessions
    if (req.http.Cookie && req.http.Cookie ~ "is_logged_in") {
        set req.http.X-Debug = "No Cache for logged in users";
        return (pass);
    }

    if (req.url ~ "\.(css|js|gif|jpe?g|bmp|png|tiff?|ico|img|tga|wmf|svg|swf|ico|mp3|mp4|m4a|ogg|mov|avi|wmv|zip|gz|pdf|ttf|eot|wof)$") {
        return (hash);
    }

    // If it passes all these tests, do a lookup anyway.
    return (hash);

}

sub vcl_backend_response {

    # allowing to purge
    set beresp.http.X-Ban-Url = bereq.url;
    set beresp.http.X-Ban-Host = bereq.http.host;
    set beresp.http.X-Debug = bereq.http.X-Debug;

   if (bereq.url ~ "^(/var/([^/]+/)?cache/public/|/var/cache/textoimage/|/design/|/share/icons/|/extension/[^/]+/design/|/var/([^/]+/)?storage/images/).+\.(gif|jpeg|jpg|swf|css|js|png|zip|gz|pdf|ico|bmp|tiff?|tga|svg|mp3|mp4|m4a|ogg|mov|avi|wmv)$") {
        unset beresp.http.Set-Cookie;
        # vedi http://www.mugo.ca/Blog/Using-Varnish-to-speed-up-eZ-Publish-websites
        set beresp.ttl = 31d;
        set beresp.http.Expires = "" + (now + beresp.ttl);
        set beresp.http.X-Ttl = "31d";
   }
   // We can use VCL to make Varnish keep all objects for X minutes (beresp.grace + beresp.keep) beyond their TTL with a grace period of Y minutes (beresp.grace):

   // https://varnish-cache.org/docs/trunk/users-guide/vcl-grace.html#users-guide-handling-misbehaving-servers
   set beresp.grace = 1m;
   set beresp.keep  = 3m;
}

sub vcl_deliver {

    # Comment out if you'd like to send those response headers
    unset resp.http.X-Ban-Url;
    unset resp.http.X-Ban-Host;

    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
        set resp.http.X-Cache-Hits = obj.hits;
    } else {
        set resp.http.X-Cache = "MISS";
    }
    set resp.http.X-Served-By = server.hostname;
}

sub vcl_synth {
    if (resp.status == 850) {
       set resp.http.Location = req.http.x-redir;
       set resp.http.Location = "https://sito.comune.trento.it" + req.url;
       set resp.status = 301;
       return (deliver);
    }
}
```


download_images.php
```
<?php
require 'autoload.php';

$script = eZScript::instance( array( 'description' => ( 'Download images from endpoint' ),
                                     'use-session' => false,
                                     'use-modules' => true,
                                     'use-extensions' => true ) );

$script->startup();

$options = $script->getOptions( '[url:][vardir:][index:]',
    '',
    array(
        'url'  => 'Endpoint url',
        'vardir' => 'Remote var directory (eg ezflow_site)',
        'index' => 'Start form index'
    )
);
$script->initialize();
$script->setUseDebugAccumulators( true );

$cli = eZCLI::instance();

$output = new ezcConsoleOutput();

$nfsBasePath = '/mnt/efs/cluster-openpa/';

try
{
    $cli->warning( "Count images ... ", false );
    $db = eZDB::instance();
    $length = 50;
    $limit = array(
        'offset' => 0,
        'length' => $length
    );
    $count = (int)eZPersistentObject::count(eZImageFile::definition());
    $cli->warning( $count );

    $bar = new ezcConsoleProgressbar( $output, $count );

    $errors = array();
    do {
        $imageObjectList = eZPersistentObject::fetchObjectList(eZImageFile::definition(), null, null, null, $limit, false);
        foreach( $imageObjectList as $index => $image )
        {
            if ($index > (int)$options['index']) {
                if (strpos($image['filepath'], 'trash') === false) {
                    $filepath = $image['filepath'];

                    if ($options['url']) {
                        $remoteFilepath = $filepath;
                        if ($options['vardir']) {
                            $varDir = eZINI::instance()->variable('FileSettings', 'VarDir');
                            $remoteVarDir = $options['vardir'];
                            $remoteFilepath = str_replace($varDir, "var/$remoteVarDir", $filepath);
                        }

                        if (!file_exists($nfsBasePath . $filepath)) {
                            $name = basename($filepath);
                            $dir = $nfsBasePath . str_replace($name, '', $filepath);
                            $url = rtrim($options['url']) . '/';
                            $data = eZHTTPTool::getDataByURL($url . $remoteFilepath);
                            if (!empty($data) && strpos($data, '404 Not Found') === false) {
                                if (eZFile::create($name, $dir, $data))
                                    $result = 'ok';
                                else
                                    $result = 'Error while creating local file';
                                $errors[$result][] = $filepath;
                            } else {
                                $result = 'Remote file not found';
                                $errors[$result][] = $filepath;
                            }
                        } else {
                            $result = 'Local file already exists';
                        }
                    } else {
                        $cli->notice($image['filepath']);
                    }
                } else {
                    $result = 'Image in trash?';
                    $errors[$result][] = $filepath;
                }
            }
            $bar->setOptions(array('formatString'=> "%act% / %max% [%bar%] %fraction%%"));
            $bar->advance();
        }
        $limit['offset'] += $length;
    } while ( count( $imageObjectList ) == $length );

    $bar->finish();

    $output->outputLine();

    foreach( $errors as $key => $value ){
        $items = array_unique($value);
        $trans = \eZCharTransform::instance();
        $filename = 'download_images-' . $trans->transformByGroup( $key, 'identifier' ) . '.log';
        foreach($items as $item){
            eZLog::write($item, $filename);
        }
        $cli->error($key . ' => ' . count($items));
    }

    $script->shutdown();
}
catch( Exception $e )
{
    $errCode = $e->getCode();
    $errCode = $errCode != 0 ? $errCode : 1; // If an error has occured, script must terminate with a status other than 0
    $script->shutdown( $errCode, $e->getMessage() );
}
```