# Infrastruttura

## Attivare Memcached in php.ini
```
session.save_handler = memcached
session.save_path = localhost:11211
```

# Per istanza

## Database

Vedi update.sql

## Solr

Aggiungere in `schema.xml` di ciascuna istanza all'interno del tag `<fields>`
```
   <!-- denormalised fields for hidden and visible path elements -->
   <field name="meta_visible_path_si" type="sint" indexed="true" stored="true" multiValued="true"/>     <!-- Visible Location path IDs -->
   <field name="meta_visible_path_string_ms" type="mstring" indexed="true" stored="true" multiValued="true"/> <!-- Visible Location path string -->
   <field name="meta_hidden_path_si" type="sint" indexed="true" stored="true" multiValued="true"/>     <!-- Hidden Location path IDs -->
   <field name="meta_hidden_path_string_ms" type="mstring" indexed="true" stored="true" multiValued="true"/> <!-- Hidden Location path string -->
```

(non è necessario reindicizzare)